from datetime import datetime

import allure
import os
import pytest
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager

host=os.environ.get('MAIN_HOST')
selenium_port=os.environ.get('SELENIUM_PORT')

desiredCapabilities={"browserName":"chrome"}
@pytest.fixture(scope='function')
def driver():
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")
    driver = webdriver.Remote(command_executor=f"http://{host}:{selenium_port}/wd/hub", desired_capabilities=desiredCapabilities)
    yield driver
    driver.quit()
